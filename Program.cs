using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

#pragma warning disable 4014

// ReSharper disable ImplicitlyCapturedClosure

namespace TimezoneBot {
    internal class TimezoneBotException : Exception {
        private readonly string _message;

        internal TimezoneBotException(string message) {
            _message = message;
        }

        public override string ToString() => _message;
    }

    internal class Reminder {
        public DateTime Time;
        public ulong[] Users;
        public ulong[] Roles;
        public string Message;
        public ulong Channel;
        public ulong Creator;
        public short Id;

        internal Reminder(DateTime time, ulong[] users, ulong[] roles, string message, ulong channel, ulong creator, short id) {
            Time = time;
            Users = users;
            Roles = roles;
            Message = message;
            Channel = channel;
            Creator = creator;
            Id = id;
        }
    }

    internal static class Program {
        private static TimeZoneInfo CustomTimezone(string name, sbyte hours, sbyte minutes = 0) {
            var text = $"(UTC{hours:+00;-00;±00}:{minutes:00}) {name}";
            return TimeZoneInfo.CreateCustomTimeZone(name, TimeSpan.FromHours(hours) + TimeSpan.FromMinutes(minutes), text, name, name, new TimeZoneInfo.AdjustmentRule[0]);
        }

        private static readonly Dictionary<string, TimeZoneInfo> CustomTimezones = new Dictionary<string, TimeZoneInfo> {
            {"Alpha Time Zone", CustomTimezone("Alpha Time Zone", 1)},
            {"Bravo Time Zone", CustomTimezone("Bravo Time Zone", 2)},
            {"Charlie Time Zone", CustomTimezone("Charlie Time Zone", 3)},
            {"Delta Time Zone", CustomTimezone("Delta Time Zone", 4)},
            {"Echo Time Zone", CustomTimezone("Echo Time Zone", 5)},
            {"Foxtrot Time Zone", CustomTimezone("Foxtrot Time Zone", 6)},
            {"Golf Time Zone", CustomTimezone("Golf Time Zone", 7)},
            {"Hotel Time Zone", CustomTimezone("Hotel Time Zone", 8)},
            {"India Time Zone", CustomTimezone("India Time Zone", 9)},
            {"Kilo Time Zone", CustomTimezone("Kilo Time Zone", 10)},
            {"Lima Time Zone", CustomTimezone("Lima Time Zone", 11)},
            {"Mike Time Zone", CustomTimezone("Mike Time Zone", 12)},
            {"November Time Zone", CustomTimezone("November Time Zone", -1)},
            {"Oscar Time Zone", CustomTimezone("Oscar Time Zone", -2)},
            {"Papa Time Zone", CustomTimezone("Papa Time Zone", -3)},
            {"Quebec Time Zone", CustomTimezone("Quebec Time Zone", -4)},
            {"Romeo Time Zone", CustomTimezone("Romeo Time Zone", -5)},
            {"Sierra Time Zone", CustomTimezone("Sierra Time Zone", -6)},
            {"Tango Time Zone", CustomTimezone("Tango Time Zone", -7)},
            {"Uniform Time Zone", CustomTimezone("Uniform Time Zone", -8)},
            {"Victor Time Zone", CustomTimezone("Victor Time Zone", -9)},
            {"Whiskey Time Zone", CustomTimezone("Whiskey Time Zone", -10)},
            {"X-ray Time Zone", CustomTimezone("X-ray Time Zone", -11)},
            {"Yankee Time Zone", CustomTimezone("Yankee Time Zone", -12)},
            {"Zulu Time Zone", CustomTimezone("Zulu Time Zone", 0)},

            {"Coordinated Universal Time", CustomTimezone("Coordinated Universal Time", 0)},
            {"Indochina Time", CustomTimezone("Indochina Time", 7)},
            {"Indonesia Western Time", CustomTimezone("Indonesia Western Time", 7)},
            {"Waktu Indonesia Barat", CustomTimezone("Waktu Indonesia Barat", 7)}
        };

        private static readonly Dictionary<string, TimeZoneInfo> TimezoneFromAbbreviation = new Dictionary<string, TimeZoneInfo> {
            {"A", CustomTimezones["Alpha Time Zone"]},
            {"B", CustomTimezones["Bravo Time Zone"]},
            {"C", CustomTimezones["Charlie Time Zone"]},
            {"D", CustomTimezones["Delta Time Zone"]},
            {"E", CustomTimezones["Echo Time Zone"]},
            {"F", CustomTimezones["Foxtrot Time Zone"]},
            {"G", CustomTimezones["Golf Time Zone"]},
            {"H", CustomTimezones["Hotel Time Zone"]},
            {"I", CustomTimezones["India Time Zone"]},
            {"K", CustomTimezones["Kilo Time Zone"]},
            {"L", CustomTimezones["Lima Time Zone"]},
            {"M", CustomTimezones["Mike Time Zone"]},
            {"N", CustomTimezones["November Time Zone"]},
            {"O", CustomTimezones["Oscar Time Zone"]},
            {"P", CustomTimezones["Papa Time Zone"]},
            {"Q", CustomTimezones["Quebec Time Zone"]},
            {"R", CustomTimezones["Romeo Time Zone"]},
            {"S", CustomTimezones["Sierra Time Zone"]},
            {"T", CustomTimezones["Tango Time Zone"]},
            {"U", CustomTimezones["Uniform Time Zone"]},
            {"V", CustomTimezones["Victor Time Zone"]},
            {"W", CustomTimezones["Whiskey Time Zone"]},
            {"X", CustomTimezones["X-ray Time Zone"]},
            {"Y", CustomTimezones["Yankee Time Zone"]},
            {"Z", CustomTimezones["Zulu Time Zone"]},

            {"ACST", TimeZoneInfo.FindSystemTimeZoneById("Cen. Australia Standard Time")},
            {"AEST", TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time")},
            {"AWST", TimeZoneInfo.FindSystemTimeZoneById("W. Australia Standard Time")},
            {"ACWST", TimeZoneInfo.FindSystemTimeZoneById("Aus Central W. Standard Time")},
            {"CST", TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time")},
            {"CEST", TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time")},
            {"EST", TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")},
            {"ICT", CustomTimezones["Indochina Time"]},
            {"GMT", TimeZoneInfo.FindSystemTimeZoneById("Greenwich Standard Time")},
            {"MST", TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time")},
            {"PST", TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time")},
            {"UTC", CustomTimezones["Coordinated Universal Time"]},
            {"WIB", CustomTimezones["Waktu Indonesia Barat"]}
        };

        private static readonly List<TimeZoneInfo> Timezones = TimezoneFromAbbreviation.Values.ToList();
        private static readonly Regex NonAlphanumericRegex = new Regex("^[^a-zA-Z0-9]+|[^a-zA-Z0-9]+$", RegexOptions.Compiled);
        private static readonly Regex IgnoreRegex = new Regex("^[^a-zA-Z]*$", RegexOptions.Compiled);
        private static readonly Regex NumberWordRegex = new Regex(@"-?\d+(?:.\d+)?|[a-zA-Z]+", RegexOptions.Compiled);
        private const string ConfigurationPath = "bot.cfg";
        private const string ConfigurationBackupPath = "bot.cfg.bak";
        private static List<string> _owners = new List<string>();
        private static string _prefix = "?";
        private static string _token;
        private static CancellationTokenSource _canceller = new CancellationTokenSource();
        private static readonly List<Reminder> Reminders = new List<Reminder>();
        private static readonly Dictionary<short, Reminder> ReminderLookup = new Dictionary<short, Reminder>();
        private static readonly Dictionary<ulong, string> UserTimezones = new Dictionary<ulong, string>();
        private static readonly Dictionary<ulong, ulong> ChannelFilters = new Dictionary<ulong, ulong>();

        private static DiscordSocketClient _bot;

        private static async Task Main() {
            UpdateLoop();
            Configure();
            _bot = new DiscordSocketClient();
            _bot.Log += msg => {
                Console.WriteLine(msg.ToString());
                return Task.CompletedTask;
            };
            await _bot.LoginAsync(TokenType.Bot, _token);
            await _bot.StartAsync();
            await _bot.SetGameAsync($"with time | {_prefix}help");
            _bot.MessageReceived += MessageReceived;
            _bot.Ready += () => {
                Task.Factory.StartNew(() => { Remind(_canceller.Token); }, _canceller.Token);
                return Task.CompletedTask;
            };
            await Task.Delay(-1);
        }

        private static async void UpdateLoop() {
            if (Environment.OSVersion.Platform != PlatformID.Unix && Environment.OSVersion.Platform != PlatformID.MacOSX) return;
            await Task.Delay(TimeSpan.FromHours(4));
            Process.Start("./bin.sh");
            Environment.Exit(0);
        }

        private static void Configure() {
            if (!File.Exists(ConfigurationPath)) {
                if (File.Exists(ConfigurationBackupPath)) File.Move(ConfigurationBackupPath, ConfigurationPath);
                else {
                    ConfigurationDialog();
                    return;
                }
            }
            if (File.Exists(ConfigurationBackupPath)) File.Delete(ConfigurationBackupPath);
            foreach (var line in File.ReadLines(ConfigurationPath)) {
                var parts = line.Split("=");
                switch (parts[0]) {
                    case "prefix":
                        _prefix = parts[1];
                        break;

                    case "usertimezones":
                        if (!string.IsNullOrEmpty(parts[1])) foreach (var kv in parts[1].Split(", ").Select(item => item.Split(": "))) UserTimezones[ulong.Parse(kv[0])] = kv[1];
                        break;

                    case "channelfilters":
                        if (!string.IsNullOrEmpty(parts[1])) foreach (var kv in parts[1].Split(", ").Select(item => item.Split(": "))) ChannelFilters[ulong.Parse(kv[0])] = ulong.Parse(kv[1]);
                        break;

                    case "reminders":
                        if (!string.IsNullOrEmpty(parts[1]))
                            foreach (var reminderArgs in parts[1].Split(" \"\" ").Select(item => item.Split(" \" "))) {
                                var reminder = new Reminder(new DateTime(long.Parse(reminderArgs[0])), reminderArgs[1].Split(", ").Where(s => s != "").Select(ulong.Parse).ToArray(), reminderArgs[2].Split(", ").Where(s => s != "").Select(ulong.Parse).ToArray(), reminderArgs[3], ulong.Parse(reminderArgs[4]), ulong.Parse(reminderArgs[5]), short.Parse(reminderArgs[6]));
                                Reminders.Add(reminder);
                            }
                        break;

                    case "owners":
                        _owners = parts[1].Split(", ").ToList();
                        break;

                    case "token":
                        _token = parts[1];
                        break;
                }
            }
        }

        private static void ConfigurationDialog() {
            Console.Write("What is your Discord ID? (This is in the form <username>#<number>) ");
            _owners = new List<string>(new[] {Console.ReadLine()});
            Console.Write("What is your Discord bot token? ");
            _token = Console.ReadLine();
            Console.Write("What is the prefix for this bot? (If you press enter, it will be '?') ");
            _prefix = Console.ReadLine();
            _prefix = _prefix.Length == 0 ? "?" : _prefix;
            SaveConfiguration();
        }

        private static void SaveConfiguration() {
            var tempFile = Path.GetTempFileName();
            File.AppendAllText(ConfigurationPath, "");
            File.WriteAllText(tempFile, $@"prefix={_prefix}
usertimezones={string.Join(", ", UserTimezones.Select(kvp => $"{kvp.Key.ToString()}: {kvp.Value}"))}
channelfilters={string.Join(", ", ChannelFilters.Select(kvp => $"{kvp.Key.ToString()}: {kvp.Value.ToString()}"))}
reminders={string.Join(" \"\" ", Reminders.Select(reminder => string.Join(" \" ", reminder.Time.Ticks.ToString(), string.Join(", ", reminder.Users.Select(user => user.ToString())), string.Join(", ", reminder.Roles.Select(role => role.ToString())), reminder.Message, reminder.Channel.ToString(), reminder.Creator.ToString(), reminder.Id.ToString())))}
owners={string.Join(", ", _owners)}
token={_token}");
            File.Move(ConfigurationPath, ConfigurationBackupPath);
            File.Move(tempFile, ConfigurationPath);
            File.Delete(ConfigurationBackupPath);
        }

        private static bool IsOwner(IUser user) {
            return _owners.Contains($"{user.Username}#{user.Discriminator}");
        }

        private static bool IsOwnerOrMod(IUser user) {
            return IsOwner(user) || ((SocketGuildUser) user).Roles.Any(role => role.Permissions.ManageMessages);
        }

        private static async Task MessageReceived(SocketMessage message) {
            var content = message.Content.ToLower().TrimStart();
            if (content.StartsWith(_prefix)) {
                content = content.Substring(_prefix.Length).Trim();
                if (IgnoreRegex.IsMatch(content)) return;
                var command = content.Split()[0];
                if (IgnoreRegex.IsMatch(command)) return;
                var words = message.Content.Substring(_prefix.Length).Trim().Split().Skip(1).ToArray();
                short id;
                string result;
                switch (command) {
                    case "n":
                    case "t":
                    case "now":
                    case "time":
                        try {
                            if (command[0] == 't' && words.Length != 0 && words[0] == "in") result = Time(TimeIn(words.Skip(1)), message.Channel);
                            else result = Time(message.Channel);
                        } catch (TimezoneBotException e) {
                            result = e.ToString();
                        } catch (Exception e) {
                            Console.WriteLine(e.ToString());
                            result = "Unknown error encountered.";
                        }
                        if (string.IsNullOrEmpty(result)) result = "No users have set a timezone.";
                        break;

                    case "help":
                    case "h":
                        result = $@"
Commands:
```
{_prefix}[h|help]             :  Display this help message.
{_prefix}[t|n|time|now]       :  Give the current time in configured timezones.
{_prefix}[t in|time in]       :  Give the time after a certain time interval.
{_prefix}[c|convert]          :  Convert the specified time from one timezone to another.
{_prefix}[z|zones]            :  List the timezones used by `now`.
{_prefix}[s|set]              :  Sets your timezone.
{_prefix}[u|unset]            :  Unsets your timezone.
{_prefix}[l|listzones]        :  Lists all timezones in a PM.
{_prefix}[r|remind]           :  Ping you at a certain time with a certain message, given in double quotes.
{_prefix}[rr|remind role]     :  Ping a role at a certain time with a certain message, given in double quotes. This will only work if you are mod, owner or have the role.
{_prefix}[su|subscribe]       :  Subscribe to a reminder.
{_prefix}[sur|subscribe role] :  Subscribe a role to a reminder. This will only work if you are mod, owner or have the role.
{_prefix}[lr|listreminders]   :  List reminders.
```
Owner/mod commands:
```
{_prefix}[sf|set filter]      :  Sets user filter role for the current channel.
{_prefix}[uf|unset filter]    :  Unsets user filter role for the current channel.
{_prefix}[ur|unremind]        :  Remove a reminder. If the message author created the reminder they can also remove it.
{_prefix}[re|restart]         :  Restart the bot, updating if there are any updates available.
```";
                        break;

                    case "c":
                    case "convert":
                        try {
                            result = Convert(words, message.Channel, message.Author);
                        } catch (TimezoneBotException e) {
                            result = e.ToString();
                        } catch (Exception e) {
                            Console.WriteLine(e.ToString());
                            result = "Unknown error encountered.";
                        }
                        if (string.IsNullOrEmpty(result)) result = "No users have set a timezone.";
                        break;

                    case "s":
                    case "sf":
                    case "set":
                        if (words.Length != 0 && words[0] == "filter") {
                            words = words.Skip(1).ToArray();
                            command = "sf";
                        }
                        if (command == "sf") {
                            if (!IsOwnerOrMod(message.Author)) {
                                result = "Insufficient permissions.";
                                break;
                            }
                            var roleName = string.Join(' ', words);
                            if (((IGuildChannel) message.Channel).Guild.Roles.Any(role => role.Name == roleName)) {
                                ChannelFilters[message.Channel.Id] = ((IGuildChannel) message.Channel).Guild.Roles.First(role => role.Name == roleName).Id;
                                SaveConfiguration();
                                result = "Filter set successfully.";
                            } else result = "Role does not exist.";
                            break;
                        }
                        result = "Timezone successfully set.";
                        if (TimezoneFromAbbreviation.ContainsKey(words[0].ToUpper())) {
                            UserTimezones[message.Author.Id] = words[0].ToUpper();
                            SaveConfiguration();
                        } else {
                            try {
                                TimeZoneInfo.FindSystemTimeZoneById(words[0]);
                                UserTimezones[message.Author.Id] = words[0];
                                SaveConfiguration();
                            } catch {
                                result = "Unknown timezone.";
                            }
                        }
                        break;

                    case "u":
                    case "uf":
                    case "unset":
                        if (words.Length != 0 && words[0] == "filter") command = "uf";
                        if (command == "uf") {
                            if (!IsOwnerOrMod(message.Author)) {
                                result = "Insufficient permissions.";
                                break;
                            }
                            if (ChannelFilters.ContainsKey(message.Channel.Id)) {
                                ChannelFilters.Remove(message.Channel.Id);
                                SaveConfiguration();
                                result = "Filter unset successfully.";
                            } else result = "Channel does not have a filter set.";
                            break;
                        }
                        result = "Timezone successfully unset.";
                        if (UserTimezones.ContainsKey(message.Author.Id)) {
                            UserTimezones.Remove(message.Author.Id);
                            SaveConfiguration();
                        } else result = "User does not have a timezone set.";
                        break;

                    case "z":
                    case "zones":
                        result = Zones(message.Channel);
                        if (string.IsNullOrEmpty(result)) result = "No users have set a timezone.";
                        break;

                    case "l":
                    case "listzones":
                        await message.Author.SendMessageAsync(ListZones());
                        return;

                    case "r":
                    case "rr":
                    case "remind":
                        if (words.Length != 0 && words[0] == "role") {
                            command = "rr";
                            words = words.Skip(1).ToArray();
                        }
                        ulong roleId = 0;
                        var remindRole = command == "rr";
                        var parts = string.Join(' ', words).Split('"');
                        if (remindRole) {
                            if (parts.Length < 3) {
                                result = "Role not found or not double quoted.";
                                break;
                            }
                            var roleName = parts[1];
                            roleId = ((IGuildChannel) message.Channel).Guild.Roles.FirstOrDefault(role => role.Name != roleName)?.Id ?? 0;
                            if (!IsOwnerOrMod(message.Author) && ((IGuildUser) message.Author).RoleIds.All(userRoleId => roleId != userRoleId)) {
                                result = "Insufficient permissions to remind role.";
                                break;
                            }
                            if (roleId == 0) {
                                result = "Role does not exist.";
                                break;
                            }
                            words = string.Join('"', parts.Skip(2)).Split(' ');
                        }
                        var remindIn = words.LongLength != 0 && words[0] == "in";
                        if (remindIn) words = words.Skip(1).ToArray();
                        if (remindRole) parts = string.Join(' ', words).Split('"');
                        if (parts.Length < 3) {
                            result = "Message not found or not double quoted.";
                            break;
                        }
                        DateTime time;
                        if (remindIn) {
                            time = DateTime.Now + ParseTimeSpan(parts[0].TrimEnd());
                        } else {
                            time = ParseTime(parts[0].TrimEnd());
                            time -= GetTimezone(UserTimezones[message.Author.Id]).GetUtcOffset(time);
                        }
                        var reminderMessage = parts[1];
                        if (words.Length < 2) {
                            result = "Insufficient information.";
                            break;
                        }
                        id = 0;
                        while (ReminderLookup.ContainsKey(id)) id++;
                        var reminder = new Reminder(time, remindRole ? new ulong[] { } : new[] {message.Author.Id}, remindRole ? new[] {roleId} : new ulong[] { }, reminderMessage, message.Channel.Id, message.Author.Id, id);
                        Reminders.Add(reminder);
                        SaveConfiguration();
                        ReminderLookup[id] = reminder;
                        _canceller.Cancel();
                        _canceller = new CancellationTokenSource();
                        Task.Factory.StartNew(() => { Remind(_canceller.Token); }, _canceller.Token);
                        result = $"Reminder added (ID {id}).";
                        break;

                    case "su":
                    case "sur":
                    case "subscribe":
                        if (words.Length != 0 && words[0] == "role") {
                            command = "sur";
                            words = words.Skip(1).ToArray();
                        }
                        id = short.Parse(words[0]);
                        var name = string.Join(' ', words.Skip(1));
                        if (command == "sur") {
                            roleId = ((IGuildChannel) message.Channel).Guild.Roles.FirstOrDefault(role => role.Name == name)?.Id ?? 0;
                            if (ReminderLookup[id].Roles.Contains(roleId)) {
                                result = "Role already subscribed to reminder.";
                                break;
                            }
                            if (!IsOwnerOrMod(message.Author) && ((IGuildUser) message.Author).RoleIds.All(userRoleId => roleId != userRoleId)) {
                                result = "Insufficient permissions to remind role.";
                                break;
                            }
                            if (roleId == 0) {
                                result = "Role does not exist.";
                                break;
                            }
                            ReminderLookup[id].Roles = ReminderLookup[id].Roles.Concat(new[] {roleId}).ToArray();
                            SaveConfiguration();
                            result = "Role subscribed to reminder.";
                            break;
                        }
                        if (ReminderLookup[id].Users.Contains(message.Author.Id)) {
                            result = "Already subscribed to reminder.";
                            break;
                        }
                        ReminderLookup[id].Users = ReminderLookup[id].Users.Concat(new[] {message.Author.Id}).ToArray();
                        SaveConfiguration();
                        result = "Subscribed to reminder.";
                        break;

                    case "ur":
                    case "unremind":
                        id = short.Parse(words[0]);
                        if (!ReminderLookup.ContainsKey(id)) {
                            result = "No reminder with given ID.";
                            break;
                        }
                        if (message.Author.Id != ReminderLookup[id].Creator && !IsOwnerOrMod(message.Author)) {
                            result = "Insufficient permissions to remove reminder.";
                            break;
                        }
                        Reminders.RemoveAll(r => r.Id == id);
                        SaveConfiguration();
                        ReminderLookup.Remove(id);
                        result = "Reminder removed.";
                        _canceller.Cancel();
                        _canceller = new CancellationTokenSource();
                        Task.Factory.StartNew(() => { Remind(_canceller.Token); }, _canceller.Token);
                        break;

                    case "lr":
                    case "listreminders":
                        var guildChannel = (IGuildChannel) message.Channel;
                        result = string.Join('\n', Reminders.Select(r => $"ID {r.Id}: \"{r.Message}\" at {r.Time}, subscribed to by {string.Join(", ", r.Users.Select(user => Username(user, guildChannel).Result).Concat(r.Roles.Select(role => '@' + guildChannel.Guild.Roles.First(guildRole => guildRole.Id == role).Name)))} (created by {Username(r.Creator, guildChannel).Result})"));
                        if (string.IsNullOrEmpty(result)) result = "No reminders.";
                        break;

                    case "re":
                    case "restart":
                        if (!IsOwnerOrMod(message.Author)) {
                            result = "Insufficient permissions to restart bot.";
                            break;
                        }
                        if (Environment.OSVersion.Platform != PlatformID.Unix && Environment.OSVersion.Platform != PlatformID.MacOSX) {
                            result = "Auto-updates do not yet work on non-Unix devices.";
                            break;
                        }
                        Process.Start("./run.sh");
                        await message.Channel.SendMessageAsync("Bot restarted");
                        Environment.Exit(0);
                        result = "";
                        break;

                    default:
                        result = "Command not recognized.";
                        break;
                }
                await message.Channel.SendMessageAsync(result);
            }
        }

        private static async void Remind(CancellationToken token) {
            if (Reminders.Count == 0) return;
            var reminder = Reminders.Aggregate((left, right) => left.Time < right.Time ? left : right);
            var channel = (SocketTextChannel) _bot.GetChannel(reminder.Channel);
            if ((reminder.Time - DateTime.Now).Hours < 0 || (reminder.Time - DateTime.Now).Minutes < -5) {
                Reminders.Remove(reminder);
                ReminderLookup.Remove(reminder.Id);
                SaveConfiguration();
            } else if (reminder.Time < DateTime.Now) {
                await channel.SendMessageAsync($"{string.Join(' ', reminder.Users.Select(user => channel.GetUser(user).Mention).Concat(reminder.Roles.Select(role => channel.Guild.Roles.First(reminderRole => reminderRole.Id == role).Mention)))} {reminder.Message}");
                Reminders.Remove(reminder);
                ReminderLookup.Remove(reminder.Id);
                SaveConfiguration();
            } else {
                try {
                    await Task.Delay(reminder.Time - DateTime.Now, token);
                } catch {
                    return;
                }
                await channel.SendMessageAsync($"{string.Join(' ', reminder.Users.Select(user => channel.GetUser(user).Mention).Concat(reminder.Roles.Select(role => channel.Guild.Roles.First(reminderRole => reminderRole.Id == role).Mention)))} {reminder.Message}");
                Reminders.Remove(reminder);
                ReminderLookup.Remove(reminder.Id);
                SaveConfiguration();
            }
            Task.Factory.StartNew(() => { Remind(token); }, token);
        }

        private static TimeZoneInfo GetTimezone(string id) {
            try {
                return CustomTimezones.ContainsKey(id)
                    ? CustomTimezones[id]
                    : TimezoneFromAbbreviation.ContainsKey(id.ToUpper())
                        ? TimezoneFromAbbreviation[id.ToUpper()]
                        : TimeZoneInfo.FindSystemTimeZoneById(id);
            } catch {
                throw new TimezoneBotException("Unknown timezone.");
            }
        }

        private static DateTime TimeIn(IEnumerable<string> words) {
            return DateTime.Now + ParseTimeSpan(words);
        }

        private static Dictionary<string, List<string>> ParseCommand(IEnumerable<string> words, string[] keywords,
            string key) {
            var result = new Dictionary<string, List<string>> {[key] = new List<string>()};
            foreach (var keyword in keywords) {
                result[keyword] = new List<string>();
            }
            foreach (var item in words) {
                var word = NonAlphanumericRegex.Replace(item, string.Empty);
                if (keywords.Contains(word)) {
                    key = word;
                } else {
                    result[key].Add(item);
                }
            }
            return result;
        }

        private static string Convert(IEnumerable<string> words, IChannel channel, IUser author) {
            var data = ParseCommand(words, new[] {"from", "to"}, "time");
            TimeZoneInfo from, to;
            try {
                from = GetTimezone(data["from"].Count > 0 ? string.Join(' ', data["from"]) : UserTimezones.ContainsKey(author.Id) ? UserTimezones[author.Id] : "UTC");
                to = data["to"].Count > 0 ? TimezoneFromAbbreviation[data["to"][0].ToUpper()] : null;
            } catch {
                throw new TimezoneBotException("Invalid time zones.");
            }
            var time = ParseTime(data["time"]);
            return to == null
                ? Time(
                    time + TimeZoneInfo.Local.BaseUtcOffset - from.BaseUtcOffset -
                    (from.IsDaylightSavingTime(time) ? TimeSpan.FromHours(1) : TimeSpan.Zero), channel)
                : DisplayDate(time + to.BaseUtcOffset - from.BaseUtcOffset);
        }

        private static TimeSpan ParseTimeSpan(string s) => ParseTimeSpan(new[] {s});

        private static TimeSpan ParseTimeSpan(IEnumerable<string> words) {
            var span = new TimeSpan();
            var wordList = NumberWordRegex.Matches(string.Join(' ', words)).Select(match => match.Value).ToArray();
            if (wordList.Length == 1 && wordList[0].Contains(":")) {
                var parts = wordList[0].Split(":");
                span += TimeSpan.FromHours(int.Parse(parts[0]));
                span += TimeSpan.FromMinutes(int.Parse(parts[1]));
                if (parts.Length == 3) {
                    var sms = parts[2].Split(".");
                    span += TimeSpan.FromSeconds(int.Parse(sms[0]));
                    if (sms.Length > 1) {
                        span += TimeSpan.FromMilliseconds(int.Parse(sms[1].Substring(0, 3)));
                    }
                }
            }
            var previous = "";
            foreach (var item in wordList) {
                var word = NonAlphanumericRegex.Replace(item, string.Empty);
                switch (word) {
                    case "ms":
                    case "msec":
                    case "msecs":
                    case "millisecond":
                    case "milliseconds":
                        span += TimeSpan.FromMilliseconds(int.Parse(previous));
                        break;
                    case "s":
                    case "sec":
                    case "secs":
                    case "second":
                    case "seconds":
                        span += TimeSpan.FromSeconds(int.Parse(previous));
                        break;
                    case "m":
                    case "min":
                    case "mins":
                    case "minute":
                    case "minutes":
                        span += TimeSpan.FromMinutes(int.Parse(previous));
                        break;
                    case "h":
                    case "hr":
                    case "hrs":
                    case "hour":
                    case "hours":
                        span += TimeSpan.FromHours(int.Parse(previous));
                        break;
                    case "d":
                    case "day":
                    case "days":
                        span += TimeSpan.FromDays(int.Parse(previous));
                        break;
                }
                previous = word;
            }
            return span;
        }

        private static DateTime ParseTime(string s) => ParseTime(new[] {s});

        private static DateTime ParseTime(IEnumerable<string> words) {
            var s = string.Join(' ', words);
            try {
                return DateTime.Parse(s + " GMT");
            } catch {
                try {
                    return DateTime.Parse(s);
                } catch {
                    throw new TimezoneBotException("The time could not be parsed.");
                }
            }
        }

        private static string DisplayDate(DateTime date) {
            return $"{date:hh:mm tt yyyy-MM-dd}";
        }

        private static string Time(IChannel channel) {
            return Time(DateTime.Now, channel);
        }

        private static string Time(DateTime time, IChannel channel) {
            var guildChannel = (IGuildChannel) channel;
            ulong filter = 0;
            if (ChannelFilters.ContainsKey(channel.Id)) filter = ChannelFilters[channel.Id];
            return UserTimezones.Count == 0
                ? ""
                : "```\n" + string.Join(
                      "\n",
                      Timezones.Where(timezone => UserTimezones.Any(kvp => Equals(GetTimezone(kvp.Value), timezone) && (filter == 0 || ((SocketGuildUser) guildChannel.GetUserAsync(kvp.Key).Result).Roles.Any(role => role.Id == filter)) && ((SocketGuildChannel) channel).Users.Any(user => user.Id == kvp.Key)))
                          .OrderBy(timezone => timezone.GetUtcOffset(time))
                          .Select(timezone => $"{timezone.DisplayNameAt(time),-39}:  {TimeZoneInfo.ConvertTime(time, timezone):hh:mm tt yyyy-MM-dd} ({string.Join(", ", UserTimezones.Where(kvp => Equals(GetTimezone(kvp.Value), timezone) && (filter == 0 || ((SocketGuildUser) guildChannel.GetUserAsync(kvp.Key).Result).Roles.Any(role => role.Id == filter)) && ((SocketGuildChannel) channel).Users.Any(user => user.Id == kvp.Key)).Select(kvp => Username(kvp.Key, guildChannel).Result))})")
                  ) + "\n```";
        }

        private static string Zones(IChannel channel) {
            var guildChannel = (IGuildChannel) channel;
            ulong filter = 0;
            if (ChannelFilters.ContainsKey(channel.Id)) filter = ChannelFilters[channel.Id];
            var now = DateTime.Now;
            return UserTimezones.Count == 0
                ? ""
                : string.Join(
                    "\n",
                    TimezoneFromAbbreviation.Where(timezoneKvp => UserTimezones.Any(kvp => ((SocketGuildChannel) channel).Users.Any(user => user.Id == kvp.Key) && (filter == 0 || ((SocketGuildUser) guildChannel.GetUserAsync(kvp.Key).Result).Roles.Any(role => role.Id == filter) && Equals(GetTimezone(kvp.Value), timezoneKvp.Value))))
                        .OrderBy(timezoneKvp => timezoneKvp.Value.GetUtcOffset(now))
                        .Select(timezoneKvp => $"{timezoneKvp.Value.DisplayNameAt(now)} ({timezoneKvp.Key})")
                );
        }

        private static string ListZones() {
            var now = DateTime.Now;
            return string.Join("\n", TimezoneFromAbbreviation.Select(timezoneKvp => $"{timezoneKvp.Value.DisplayNameAt(now)} ({timezoneKvp.Key})"));
        }

        private static async Task<string> Username(ulong id, IChannel channel) {
            var user = await ((IGuildChannel) channel).GetUserAsync(id);
            return string.IsNullOrEmpty(user.Nickname) ? user.Username : user.Nickname;
        }

        private static string DisplayNameAt(this TimeZoneInfo timezone, DateTime time) {
            return $"(UTC{timezone.GetUtcOffset(time).Hours:+00;-00;±00}:{timezone.GetUtcOffset(time).Minutes:00}) {(timezone.IsDaylightSavingTime(time) ? timezone.DaylightName : timezone.StandardName)}";
        }
    }
}